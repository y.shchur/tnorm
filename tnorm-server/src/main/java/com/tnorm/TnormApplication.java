package com.tnorm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(exclude = { HibernateJpaAutoConfiguration.class },
		scanBasePackages = { "com.tnorm" })
public class TnormApplication {

	public static void main(String[] args) {
		SpringApplication.run(TnormApplication.class, args);
	}
}
