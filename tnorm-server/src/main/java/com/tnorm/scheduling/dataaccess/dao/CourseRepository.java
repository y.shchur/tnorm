package com.tnorm.scheduling.dataaccess.dao;


import org.springframework.data.jpa.repository.JpaRepository;

import com.tnorm.scheduling.dataaccess.model.CourseEntity;

public interface CourseRepository extends JpaRepository<CourseEntity, Long> {

}
