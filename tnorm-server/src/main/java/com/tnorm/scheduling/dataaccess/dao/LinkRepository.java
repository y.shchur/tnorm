package com.tnorm.scheduling.dataaccess.dao;

import com.tnorm.scheduling.dataaccess.model.LinkEntity;
import com.tnorm.scheduling.dataaccess.model.LinkId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface LinkRepository extends JpaRepository<LinkEntity, LinkId> {

  @Query("SELECT link FROM LinkEntity link WHERE link.start.id = :stopPointId")
  List<LinkEntity> findNextLinksByStopPointId(@Param("stopPointId") Long stopPointId);

  @Query("SELECT link FROM LinkEntity link WHERE link.id IN :ids")
  List<LinkEntity> findLinksByIds(@Param("ids") List<LinkId> ids);

}
