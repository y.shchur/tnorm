package com.tnorm.scheduling.dataaccess.dao;

import com.tnorm.scheduling.dataaccess.model.RouteEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RouteRepository extends JpaRepository<RouteEntity, Long> {

}
