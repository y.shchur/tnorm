package com.tnorm.scheduling.dataaccess.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tnorm.scheduling.dataaccess.model.SquadEntity;

public interface ServiceRepository extends JpaRepository<SquadEntity, Long> {

}
