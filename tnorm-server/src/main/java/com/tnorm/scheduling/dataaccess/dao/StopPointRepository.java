package com.tnorm.scheduling.dataaccess.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tnorm.scheduling.dataaccess.model.StopPointEntity;

public interface StopPointRepository extends JpaRepository<StopPointEntity, Long> {

}
