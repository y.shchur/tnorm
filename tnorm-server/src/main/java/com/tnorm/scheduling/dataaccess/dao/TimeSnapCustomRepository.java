package com.tnorm.scheduling.dataaccess.dao;

import com.tnorm.scheduling.dataaccess.model.LinkEntity;
import com.tnorm.scheduling.logic.api.to.MedianCalculationCriteria;

import java.time.Duration;
import java.util.Map;

public interface TimeSnapCustomRepository {

	Map<LinkEntity, Duration> getMedian(MedianCalculationCriteria criteria);
}
