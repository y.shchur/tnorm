package com.tnorm.scheduling.dataaccess.dao;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.group.GroupBy;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;
import com.tnorm.scheduling.dataaccess.model.LinkEntity;
import com.tnorm.scheduling.dataaccess.model.QTimeSnapEntity;
import com.tnorm.scheduling.logic.api.to.MedianCalculationCriteria;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.Duration;
import java.util.Map;

import static org.springframework.util.StringUtils.isEmpty;

@Repository
public class TimeSnapCustomRepositoryImpl implements TimeSnapCustomRepository {
  @PersistenceContext
  protected EntityManager entityManager;

  public Map<LinkEntity, Duration> getMedian(MedianCalculationCriteria criteria) {
    BooleanBuilder builder = new BooleanBuilder();
    QTimeSnapEntity timesnap = QTimeSnapEntity.timeSnapEntity;

    if (!isEmpty(criteria.getSinceTime())) {
      builder.and(timesnap.snapTime.after(criteria.getSinceTime())).or(timesnap.snapTime.eq(criteria.getSinceTime()));
    }
    if (!isEmpty(criteria.getTillTime())) {
      builder.and(timesnap.snapTime.before(criteria.getTillTime()));
    }
    if (!isEmpty(criteria.getDayType())) {
      builder.and(timesnap.dayType.eq(criteria.getDayType()));
    }

    JPAQuery<Duration> query = new JPAQuery<>(entityManager);
    Map<LinkEntity, Duration> results = query.from(timesnap)
        .where(builder.getValue())
        .groupBy(timesnap.link)
        .transform(GroupBy.groupBy(timesnap.link).as(Expressions.template(Duration.class, "MEDIAN({0})", timesnap.snapLinkTime)));

    return results;
  }

}
