package com.tnorm.scheduling.dataaccess.dao;

import com.tnorm.scheduling.dataaccess.model.TimeSnapEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TimeSnapRepository extends JpaRepository<TimeSnapEntity, Long>, TimeSnapCustomRepository {

}
