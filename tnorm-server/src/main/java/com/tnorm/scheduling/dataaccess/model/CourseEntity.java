package com.tnorm.scheduling.dataaccess.model;

import com.tnorm.shared.dataaccess.model.AbstractEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "COURSE")
public class CourseEntity extends AbstractEntity {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  //  @ManyToOne
  @Transient
  private RouteEntity route;

  @OneToMany(cascade = CascadeType.MERGE)
  @OrderColumn(name = "IN_COURSE_ORDER")
  @JoinColumn(name = "COURSE_ID")
  private List<TimeSnapEntity> timeSnaps = new ArrayList<>();

  @Column(name = "LINE")
  private String line;
}
