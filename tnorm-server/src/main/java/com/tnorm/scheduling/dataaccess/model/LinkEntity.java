package com.tnorm.scheduling.dataaccess.model;

import com.tnorm.shared.dataaccess.model.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "LINK")
public class LinkEntity extends AbstractEntity implements Serializable {
  private static final long serialVersionUID = 1L;

  @NonNull
  @EmbeddedId
  private LinkId id = new LinkId();

  @NonNull
  @MapsId("startId")
  @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
  @Fetch(FetchMode.JOIN)
  private StopPointEntity start;

  @NonNull
  @MapsId("endId")
  @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
  @Fetch(FetchMode.JOIN)
  private StopPointEntity end;

  public Long getStartId() {
    return (start != null) ? start.getId() : null;
  }

  public void setStartId(Long startId) {
    if (start == null) {
      start = new StopPointEntity();
    }
    start.setId(startId);
    id.setStartId(startId);
  }

  public Long getEndId() {
    return (end != null) ? start.getId() : null;
  }

  public void setEndId(Long endId) {
    if (end == null) {
      end = new StopPointEntity();
    }
    end.setId(endId);
    id.setEndId(endId);
  }
}
