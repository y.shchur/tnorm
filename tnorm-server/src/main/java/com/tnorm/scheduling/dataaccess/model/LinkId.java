package com.tnorm.scheduling.dataaccess.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Embeddable
public class LinkId implements Serializable {
  private static final long serialVersionUID = 1L;

  @NonNull
  private Long startId;

  @NonNull
  private Long endId;
}
