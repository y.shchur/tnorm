package com.tnorm.scheduling.dataaccess.model;

import com.tnorm.shared.dataaccess.model.AbstractEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "ROUTE")
public class RouteEntity extends AbstractEntity implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NonNull
  @Column(name = "LINE_NO")
  private String lineNo;

  @Column(name = "DESCRIPTION")
  private String description;

  @NonNull
  @OrderColumn
  @ManyToMany
  @JoinTable(name = "ROUTE_LINK", //
      joinColumns = {@JoinColumn(name = "ROUTE_ID")}, //
      inverseJoinColumns = {@JoinColumn(name = "LINK_END_ID"), @JoinColumn(name = "LINK_START_ID")})
  @Fetch(FetchMode.JOIN)
  private List<LinkEntity> links;
}
