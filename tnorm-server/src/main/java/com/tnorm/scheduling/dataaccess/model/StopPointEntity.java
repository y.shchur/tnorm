package com.tnorm.scheduling.dataaccess.model;

import com.tnorm.shared.dataaccess.model.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "STOP_POINT")
public class StopPointEntity extends AbstractEntity {
  private static final long serialVersionUID = 1L;

  @Id
  @NonNull
  @Column(unique = true, nullable = false)
  private Long id;

  private String name;


}
