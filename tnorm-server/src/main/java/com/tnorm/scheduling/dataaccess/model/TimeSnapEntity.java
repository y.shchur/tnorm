package com.tnorm.scheduling.dataaccess.model;

import com.tnorm.shared.dataaccess.model.AbstractEntity;
import com.tnorm.workspace.logic.api.to.DayType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "TIME_SNAP")
public class TimeSnapEntity extends AbstractEntity implements Comparable<TimeSnapEntity> {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
  @JoinColumn(name = "STOP_POINT_ID")
  private StopPointEntity stopPoint;

  @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
  private LinkEntity link;

  @Enumerated(EnumType.STRING)
  @Column(name = "DAY_TYPE")
  private DayType dayType;

  @Column(name = "DATE")
  private LocalDate date;

  @Column(name = "SCHEDULED_TIME")
  private LocalTime scheduledTime;

  @Column(name = "SNAP_TIME")
  private LocalTime snapTime;

  @Column(name = "PUNCTUALITY")
  private Duration punctuality;

  @Column(name = "SCHEDULED_LINK_TIME")
  private Duration scheduledLinkTime;

  @Column(name = "SNAP_LINK_TIME")
  private Duration snapLinkTime;

  @Override
  public int compareTo(TimeSnapEntity o) {
    if (this.getDate().isAfter(o.getDate())) {
      return 1;
    } else if (this.getScheduledTime().isAfter(o.getScheduledTime())) {
      return 1;
    } else if (this.getSnapTime().isAfter(o.getSnapTime())) {
      return 1;
    } else {
      return -1;
    }
  }
}
