package com.tnorm.scheduling.logic.api;

import com.tnorm.scheduling.dataaccess.dao.CourseRepository;
import com.tnorm.scheduling.dataaccess.model.CourseEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
@RequiredArgsConstructor
public class CourseManager {

  private final CourseRepository courseRepo;
  private final TimeSnapManager timeSnapManager;

  public CourseEntity save(CourseEntity course) {
    course.getTimeSnaps().forEach(timeSnap -> timeSnapManager.save(timeSnap));

    return courseRepo.save(course);
  }
}
