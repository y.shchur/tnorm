package com.tnorm.scheduling.logic.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tnorm.scheduling.logic.api.to.TableModelTo;
import com.tnorm.shared.util.BaseReader;
import com.tnorm.workspace.logic.api.to.CalendarFullTo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;

@Transactional
@Service
@Slf4j
@RequiredArgsConstructor
public class CsvDataManager {

  private final CsvDataParser csvDataParser;

  public boolean importCsvTimesnapBase(MultipartFile file, String encoding, String tableModel) {
    ObjectMapper objectMapper = new ObjectMapper();

    try (InputStream inputStream = file.getInputStream();
         InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName(encoding));
         Reader bufferedReader = new BufferedReader(inputStreamReader);) {

      TableModelTo tableModelParsed = objectMapper.readValue(tableModel, TableModelTo.class);
      csvDataParser.importCourses(new BaseReader(bufferedReader).getRecords(), tableModelParsed, new CalendarFullTo());

    } catch (IOException e) {
      log.error(e.getMessage());
      return false;
    }
    return true;
  }
}
