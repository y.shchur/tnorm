package com.tnorm.scheduling.logic.api;

import com.tnorm.scheduling.dataaccess.dao.StopPointRepository;
import com.tnorm.scheduling.dataaccess.model.CourseEntity;
import com.tnorm.scheduling.dataaccess.model.LinkEntity;
import com.tnorm.scheduling.dataaccess.model.LinkId;
import com.tnorm.scheduling.dataaccess.model.RouteEntity;
import com.tnorm.scheduling.dataaccess.model.StopPointEntity;
import com.tnorm.scheduling.dataaccess.model.TimeSnapEntity;
import com.tnorm.scheduling.logic.api.to.TableColumnNumbers;
import com.tnorm.scheduling.logic.api.to.TableModelTo;
import com.tnorm.workspace.logic.api.to.CalendarFullTo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Transactional
@Service
@Slf4j
@RequiredArgsConstructor
public class CsvDataParser {
  private final StopPointRepository stopPointRepo;
  private final CourseManager courseManager;

  public void importCourses(List<CSVRecord> records, TableModelTo tableModel, CalendarFullTo calendar) {
    Map<String, CourseEntity> courses = new HashMap<>();
    TableColumnNumbers table = mapTableModelToColumnNumbers(tableModel, records.get(0));
    Map<Long, StopPointEntity> stopPoints = importStopPoints(records, table);

    for (int i = 1; i < records.size(); i++) {
      CSVRecord currentRecord = records.get(i);

      StringBuilder courseIdBuilder = new StringBuilder();
      for (Integer courseIdColumn : table.getCourseIdentityCols()) {
        courseIdBuilder.append(currentRecord.get(courseIdColumn));
      }
      String courseId = courseIdBuilder.toString();
      if (!courses.containsKey(courseId)) {
        CourseEntity course = new CourseEntity();
        course.setLine(currentRecord.get(table.getLineNoCol()));
        courses.put(courseId, course);
      }

      DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
      DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("HH:mm:ss");

      LocalDate date = LocalDate.parse(currentRecord.get(table.getDateCol()), dateFormat);
      LocalTime scheduledTime = LocalTime.parse(currentRecord.get(table.getScheduledTimeCol()), timeFormat);
      LocalTime snapTime = LocalTime.parse(currentRecord.get(table.getSnapTimeCol()), timeFormat);

      TimeSnapEntity timeSnap = TimeSnapEntity.builder()//
          .stopPoint(stopPoints.get(Long.parseLong(currentRecord.get(table.getStopPointIdCol()))))
          .date(date)
          .scheduledTime(scheduledTime)
          .snapTime(snapTime)
          .punctuality(Duration.between(scheduledTime, snapTime))
          .dayType(calendar.getDayTypeOf(date))
          .build();
      courses.get(courseId).getTimeSnaps().add(timeSnap);
    }

    courses.values().
        forEach(course -> {
          course = establishInnerConnection(course);
          List<LinkEntity> links = course.getTimeSnaps()
              .stream()
              .filter(t -> t.getLink() != null)
              .map(t -> t.getLink())
              .collect(Collectors.toList());
          if (links.size() > 0) {
            course.setRoute(new RouteEntity(course.getLine(), links));
            courseManager.save(course);
          } else {
            log.warn("No links found for course: " + course.toString());
          }
        });

  }

  private StopPointEntity importStopPoint(CSVRecord record, TableModelTo table) {
    return StopPointEntity.builder()//
        .id(Long.parseLong(record.get(table.getStopPointIdCol())))
        .name(record.get(table.getStopNameCol()))
        .build();
  }

  private CourseEntity establishInnerConnection(CourseEntity course) {
    List<TimeSnapEntity> timeSnaps = course.getTimeSnaps();
    Collections.sort(timeSnaps);
    IntStream.range(1, timeSnaps.size())//
        .forEach(i -> {
          TimeSnapEntity previousTimeSnap = timeSnaps.get(i - 1);
          TimeSnapEntity currentTimeSnap = timeSnaps.get(i);
          currentTimeSnap.setLink(
              new LinkEntity(new LinkId(
                  previousTimeSnap.getStopPoint().getId(), currentTimeSnap.getStopPoint().getId()),
                  previousTimeSnap.getStopPoint(),
                  currentTimeSnap.getStopPoint()));
          currentTimeSnap.setScheduledLinkTime(
              Duration.between(previousTimeSnap.getScheduledTime(), currentTimeSnap.getScheduledTime()));
          currentTimeSnap.setSnapLinkTime(Duration.between(previousTimeSnap.getSnapTime(), currentTimeSnap.getSnapTime()));

        });
    return course;
  }

  private Map<Long, StopPointEntity> importStopPoints(List<CSVRecord> records, TableColumnNumbers table) {
    Map<Long, StopPointEntity> stopPoints = stopPointRepo.findAll().stream()
        .collect(Collectors.toMap(StopPointEntity::getId, stopPoint -> stopPoint));

    for (int i = 1; i < records.size(); i++) {
      CSVRecord currentRecord = records.get(i);
      Long stopPointId = Long.parseLong(currentRecord.get(table.getStopPointIdCol()));

      if (!stopPoints.containsKey(stopPointId)) {
        StopPointEntity stopPoint = StopPointEntity.builder()//
            .id(stopPointId)
            .name(currentRecord.get(table.getStopNameCol()))
            .build();
        stopPoints.put(stopPoint.getId(), stopPoint);
      }
    }
    return stopPointRepo.saveAll(stopPoints.values()).stream()
        .collect(Collectors.toMap(StopPointEntity::getId, stopPoint -> stopPoint));
  }

  private TableColumnNumbers mapTableModelToColumnNumbers(TableModelTo tableModel, CSVRecord header) {
    TableColumnNumbers columnNumbers = new TableColumnNumbers();
    columnNumbers.setScheduledTimeCol(findIndexOfHeaderRecord(tableModel.getScheduledTimeCol(), header));
    columnNumbers.setSnapTimeCol(findIndexOfHeaderRecord(tableModel.getSnapTimeCol(), header));
    columnNumbers.setDateCol(findIndexOfHeaderRecord(tableModel.getDateCol(), header));

    columnNumbers.setLineNoCol(findIndexOfHeaderRecord(tableModel.getLineNoCol(), header));
    columnNumbers.setServiceCol(findIndexOfHeaderRecord(tableModel.getServiceCol(), header));

    columnNumbers.setStopNameCol(findIndexOfHeaderRecord(tableModel.getStopNameCol(), header));
    columnNumbers.setStopPointIdCol(findIndexOfHeaderRecord(tableModel.getStopPointIdCol(), header));
    columnNumbers.setDestinationStopFlagCol(findIndexOfHeaderRecord(tableModel.getDestinationStopFlagCol(), header));
    columnNumbers.setDestinationStopFlagValue(tableModel.getDestinationStopFlagValue());

    List<Integer> courseIdentity = tableModel.getCourseIdentityCols()
        .stream().map(courseIdentityCol -> findIndexOfHeaderRecord(courseIdentityCol, header))
        .collect(Collectors.toList());

    columnNumbers.setCourseIdentityCols(courseIdentity);

    return columnNumbers;
  }

  private Integer findIndexOfHeaderRecord(String record, CSVRecord header) {
    for (int i = 0; i < header.size(); i++) {
      if (header.get(i).equals(record)) {
        return i;
      }
    }
    // TODO: 2019-06-15 Throw exception
    return -1;
  }
}
