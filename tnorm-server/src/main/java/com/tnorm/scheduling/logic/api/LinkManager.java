package com.tnorm.scheduling.logic.api;

import com.tnorm.scheduling.dataaccess.dao.LinkRepository;
import com.tnorm.scheduling.dataaccess.model.LinkEntity;
import com.tnorm.scheduling.logic.api.mapper.LinkMapper;
import com.tnorm.scheduling.logic.api.to.LinkFullTo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
@RequiredArgsConstructor
@Slf4j
public class LinkManager {
  private final LinkRepository linkRepo;
  private final StopPointManager stopPointManager;
  private final LinkMapper linkMapper;

  public LinkEntity save(LinkEntity link) {
    stopPointManager.save(link.getStart());
    stopPointManager.save(link.getEnd());
    log.debug("MyCounter" + link.toString());
    return linkRepo.save(link);
  }

  public List<LinkEntity> saveAll(List<LinkEntity> links) {
    return links.stream().map(l -> save(l)).collect(Collectors.toList());
  }

  public List<LinkFullTo> findNextLinksByStopPointId(Long stopPointId) {
    return linkMapper.mapLinkEntities2LinkFullTos(linkRepo.findNextLinksByStopPointId(stopPointId));
  }

  public List<LinkFullTo> findAllLinks() {
    return linkMapper.mapLinkEntities2LinkFullTos(linkRepo.findAll());
  }


}
