package com.tnorm.scheduling.logic.api;

import com.tnorm.scheduling.dataaccess.dao.RouteRepository;
import com.tnorm.scheduling.dataaccess.model.RouteEntity;
import com.tnorm.scheduling.logic.api.mapper.RouteMapper;
import com.tnorm.scheduling.logic.api.to.RouteSavingTo;
import com.tnorm.scheduling.logic.api.to.RouteTo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
@RequiredArgsConstructor
public class RouteManager {
  private final RouteRepository routeRepo;
  private final LinkManager linkManager;
  private final RouteMapper routeMapper;

  public RouteEntity save(RouteEntity route) {
    linkManager.saveAll(route.getLinks());
    return routeRepo.save(route);
  }

  public List<RouteTo> findAllRoutes() {
    return routeMapper.mapRouteEntities2RouteTos(routeRepo.findAll());
  }

  public RouteTo saveRoute(RouteSavingTo route) {
    RouteEntity routeEntity;
    if (route.getId() != null) {
      routeEntity = routeRepo.getOne(route.getId());
    } else {
      routeEntity = new RouteEntity();
    }
    routeMapper.mapRouteSavingTo2RouteEntity(route, routeEntity);

    return routeMapper.mapRouteEntity2RouteTo(routeRepo.save(routeEntity));
  }

}
