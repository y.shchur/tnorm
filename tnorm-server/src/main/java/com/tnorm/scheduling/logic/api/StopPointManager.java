package com.tnorm.scheduling.logic.api;

import com.tnorm.scheduling.dataaccess.dao.StopPointRepository;
import com.tnorm.scheduling.dataaccess.model.StopPointEntity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Transactional
@Service
@Slf4j
@RequiredArgsConstructor
public class StopPointManager {
  private final StopPointRepository stopPointRepo;

  public StopPointEntity save(StopPointEntity stopPoint) {
    log.debug(stopPoint.toString());
    return stopPointRepo.save(stopPoint);
  }

  public Optional<StopPointEntity> findById(Long id) {
    return stopPointRepo.findById(id);
  }

  public List<StopPointEntity> save(List<StopPointEntity> stopPoints) {
    return stopPointRepo.saveAll(stopPoints);
  }

}
