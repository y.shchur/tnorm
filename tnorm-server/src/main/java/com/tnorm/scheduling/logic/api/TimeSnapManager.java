package com.tnorm.scheduling.logic.api;

import com.tnorm.scheduling.dataaccess.dao.TimeSnapRepository;
import com.tnorm.scheduling.dataaccess.model.TimeSnapEntity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
@Slf4j
@RequiredArgsConstructor
public class TimeSnapManager {

  private final LinkManager linkManager;
  private final StopPointManager stopPointManager;
  private final TimeSnapRepository timeSnapRepo;

  public TimeSnapEntity save(TimeSnapEntity timeSnap) {
    stopPointManager.save(timeSnap.getStopPoint());
    if (timeSnap.getLink() != null) {
      linkManager.save(timeSnap.getLink());
    }

    log.debug("TimeSnapEntity - " + timeSnap.toString());

    return timeSnapRepo.save(timeSnap);
  }

}
