package com.tnorm.scheduling.logic.api.mapper;

import com.tnorm.scheduling.dataaccess.model.LinkEntity;
import com.tnorm.scheduling.logic.api.to.LinkFullTo;
import com.tnorm.scheduling.logic.api.to.LinkTo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = StopPointMapper.class)
public interface LinkMapper {

  LinkTo mapLinkEntity2LinkTo(LinkEntity source);

  @Mapping(target = "id", ignore = true)
  @Mapping(target = "start", ignore = true)
  @Mapping(target = "end", ignore = true)
  LinkEntity mapLinkTo2LinkEntity(LinkTo source);

  List<LinkTo> mapLinkEntities2LinkTos(List<LinkEntity> source);

  LinkFullTo mapLinkEntity2LinkFullTo(LinkEntity source);

  List<LinkFullTo> mapLinkEntities2LinkFullTos(List<LinkEntity> source);


}
