package com.tnorm.scheduling.logic.api.mapper;

import com.tnorm.scheduling.dataaccess.model.RouteEntity;
import com.tnorm.scheduling.logic.api.to.RouteSavingTo;
import com.tnorm.scheduling.logic.api.to.RouteTo;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring", uses = LinkMapper.class)
public interface RouteMapper {

  RouteTo mapRouteEntity2RouteTo(RouteEntity source);

  List<RouteTo> mapRouteEntities2RouteTos(List<RouteEntity> source);

  RouteEntity mapRouteTo2RouteEntity(RouteTo source);

  void mapRouteSavingTo2RouteEntity(RouteSavingTo source, @MappingTarget RouteEntity routeEntity);

}
