package com.tnorm.scheduling.logic.api.mapper;

import com.tnorm.scheduling.dataaccess.model.StopPointEntity;
import com.tnorm.scheduling.logic.api.to.StopPointTo;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface StopPointMapper {

  StopPointTo mapStopPointEntity2StopPointTo(StopPointEntity source);

  StopPointEntity mapStopPointTo2StopPointEntity(StopPointTo source);
}
