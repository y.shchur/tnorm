package com.tnorm.scheduling.logic.api.to;

import lombok.Data;

@Data
public class CourseTo {

  private Long id;

  private Long routeId;

  private String line;
}