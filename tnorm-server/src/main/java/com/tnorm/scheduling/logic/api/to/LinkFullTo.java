package com.tnorm.scheduling.logic.api.to;

import lombok.Data;

@Data
public class LinkFullTo {

  private StopPointTo start;

  private StopPointTo end;
}
