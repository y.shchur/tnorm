package com.tnorm.scheduling.logic.api.to;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class LinkTo {

  private Long startId;

  private Long endId;
}
