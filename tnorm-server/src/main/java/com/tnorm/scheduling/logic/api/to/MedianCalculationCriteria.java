package com.tnorm.scheduling.logic.api.to;

import com.tnorm.workspace.logic.api.to.DayType;

import java.time.LocalTime;

public class MedianCalculationCriteria {

	private DayType dayType;
	private LocalTime sinceTime;
	private LocalTime tillTime;

	public DayType getDayType() {
		return dayType;
	}

	public LocalTime getSinceTime() {
		return sinceTime;
	}

	public LocalTime getTillTime() {
		return tillTime;
	}

	public static class Builder {
		private DayType dayType;
		private LocalTime sinceTime;
		private LocalTime tillTime;

		public Builder dayType(DayType dayType) {
			this.dayType = dayType;
			return this;
		}

		public Builder sinceTime(LocalTime sinceTime) {
			this.sinceTime = sinceTime;
			return this;
		}

		public Builder tillTime(LocalTime tillTime) {
			this.tillTime = tillTime;
			return this;
		}

		public MedianCalculationCriteria build() {
			return new MedianCalculationCriteria(this);
		}
	}

	private MedianCalculationCriteria(Builder builder) {
		this.dayType = builder.dayType;
		this.sinceTime = builder.sinceTime;
		this.tillTime = builder.tillTime;
	}
}
