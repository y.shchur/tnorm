package com.tnorm.scheduling.logic.api.to;

import lombok.Data;

import java.util.List;

@Data
public class RouteSavingTo {
  private Long id;

  private String lineNo;

  private String description;

  private List<LinkTo> links;
}
