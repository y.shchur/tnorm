package com.tnorm.scheduling.logic.api.to;

import com.tnorm.shared.api.AbstractTo;
import lombok.Data;

import java.util.List;

@Data
public class RouteTo extends AbstractTo {

  private Long id;

  private String lineNo;

  private String description;

  private List<LinkFullTo> links;
}
