package com.tnorm.scheduling.logic.api.to;

import lombok.Data;

@Data
public class StopPointTo {

  private Long id;

  private String name;
}
