package com.tnorm.scheduling.logic.api.to;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Model of table. Fields represent ordinal number. The counter should start
 * with 0.
 */
@Data
public class TableColumnNumbers {

  private int dateCol;
  private int serviceCol;
  private int lineNoCol;
  private int stopNameCol;
  private int stopPointIdCol;
  private int destinationStopFlagCol;
  private String destinationStopFlagValue;
  private int scheduledTimeCol;
  private int snapTimeCol;
  private List<Integer> courseIdentityCols = new ArrayList<>();
}
