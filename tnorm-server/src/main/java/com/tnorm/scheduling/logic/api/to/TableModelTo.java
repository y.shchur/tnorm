package com.tnorm.scheduling.logic.api.to;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Model of table. Fields represent ordinal number. The counter should start
 * with 0.
 */
@Data
public class TableModelTo {

  private String dateCol;
  private String serviceCol;
  private String lineNoCol;
  private String stopNameCol;
  private String stopPointIdCol;
  private String destinationStopFlagCol;
  private String destinationStopFlagValue;
  private String scheduledTimeCol;
  private String snapTimeCol;
  private List<String> courseIdentityCols = new ArrayList<>();
}
