package com.tnorm.scheduling.logic.api.to;

import com.tnorm.workspace.logic.api.to.DayType;
import lombok.Data;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class TimeSnapTo implements Comparable<TimeSnapTo> {

  private Long id;

  private Long stopPointId;

  private Long linkId;

  private DayType dayType;

  private LocalDate date;

  private LocalTime scheduledTime;

  private LocalTime snapTime;

  private Duration punctuality;

  private Duration scheduledLinkTime;

  private Duration snapLinkTime;

  @Override
  public int compareTo(TimeSnapTo o) {
    if (this.getDate().isAfter(o.getDate())) {
      return 1;
    } else if (this.getScheduledTime().isAfter(o.getScheduledTime())) {
      return 1;
    } else if (this.getSnapTime().isAfter(o.getSnapTime())) {
      return 1;
    } else {
      return -1;
    }
  }
}
