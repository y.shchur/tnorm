package com.tnorm.scheduling.rest.api;

import com.tnorm.scheduling.logic.api.CsvDataManager;
import com.tnorm.scheduling.logic.api.LinkManager;
import com.tnorm.scheduling.logic.api.RouteManager;
import com.tnorm.scheduling.logic.api.to.LinkFullTo;
import com.tnorm.scheduling.logic.api.to.RouteSavingTo;
import com.tnorm.scheduling.logic.api.to.RouteTo;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class SchedulingRestController {

  private final CsvDataManager csvDataManager;
  private final RouteManager routeManager;
  private final LinkManager linkManager;

  @PostMapping(SchedulingUrlConstants.IMPORT_CSV)
  public HttpStatus importCsvTableData(@RequestParam("file") MultipartFile file,
                                       @RequestParam("encoding") String encoding,
                                       @RequestParam("tableModel") String tableModel) {
    boolean result = csvDataManager.importCsvTimesnapBase(file, encoding, tableModel);
    return result ? HttpStatus.OK : HttpStatus.BAD_REQUEST;
  }

  @GetMapping(SchedulingUrlConstants.ALL_ROUTES)
  public List<RouteTo> findAllRoutes() {
    return routeManager.findAllRoutes();
  }

  @PostMapping(SchedulingUrlConstants.SAVE_ROUTE)
  public RouteTo saveRoute(RouteSavingTo route) {
    return routeManager.saveRoute(route);
  }

  @GetMapping(SchedulingUrlConstants.ALL_LINKS)
  public List<LinkFullTo> findAllLinks() {
    return linkManager.findAllLinks();
  }

  @GetMapping(SchedulingUrlConstants.NEXT_LINKS)
  public List<LinkFullTo> findNextLinks(@PathVariable("id") Long id) {
    return linkManager.findNextLinksByStopPointId(id);
  }
}
