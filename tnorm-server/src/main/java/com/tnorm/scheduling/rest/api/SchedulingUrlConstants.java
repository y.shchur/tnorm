package com.tnorm.scheduling.rest.api;

public interface SchedulingUrlConstants {
  String API_SCHEDULING = "/api/scheduling";
  String IMPORT_CSV = API_SCHEDULING + "/import";
  String ALL_ROUTES = API_SCHEDULING + "/routes";
  String SAVE_ROUTE = API_SCHEDULING + "/routes/save";
  String ALL_LINKS = API_SCHEDULING + "/links";
  String NEXT_LINKS = API_SCHEDULING + "/next_link/{id}";
}
