package com.tnorm.shared.config.multitenant;

import org.hibernate.HibernateException;
import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static com.tnorm.shared.config.multitenant.MultiTenantConstants.DEFAULT_TENANT_ID;

@Component
public class SchemaMultiTenantConnectionProvider implements MultiTenantConnectionProvider {

  @Autowired
  private DataSource dataSource;

  @Override
  public boolean isUnwrappableAs(Class unwrapType) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public <T> T unwrap(Class<T> unwrapType) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Connection getAnyConnection() throws SQLException {
    return dataSource.getConnection();
  }

  @Override
  public void releaseAnyConnection(Connection connection) throws SQLException {
    connection.close();

  }

  @Override
  public Connection getConnection(String tenantIdentifier) throws SQLException {
    String tenantId = TenantContext.getCurrentTenant();
    final Connection connection = getAnyConnection();
    try (Statement statement = connection.createStatement()) {
      if (tenantId != null) {
        statement.execute("USE " + tenantId);
      } else {
        statement.execute("USE " + DEFAULT_TENANT_ID);
      }
    } catch (SQLException e) {
      throw new HibernateException("Problem setting schema to " + tenantId, e);
    }
    return connection;
  }

  @Override
  public void releaseConnection(String tenantIdentifier, Connection connection) throws SQLException {
    try (Statement statement = connection.createStatement()) {
      statement.execute("USE " + DEFAULT_TENANT_ID);
    } catch (SQLException e) {
      throw new HibernateException("Problem setting schema to " + tenantIdentifier, e);
    }
    connection.close();
  }

  @Override
  public boolean supportsAggressiveRelease() {
    // TODO Auto-generated method stub
    return true;
  }

}
