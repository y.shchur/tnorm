package com.tnorm.shared.config.multitenant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TenantContext {

  private static final Logger LOG = LoggerFactory.getLogger(TenantContext.class.getName());

  private static ThreadLocal<String> currentTenant = new ThreadLocal<>();

  public static void setCurrentTenant(String tenantId) {
    LOG.debug("Tenant set to: " + tenantId);
    currentTenant.set(tenantId);
  }

  public static String getCurrentTenant() {
    return currentTenant.get();
  }

  public static void clear() {
    currentTenant.remove();
  }
}
