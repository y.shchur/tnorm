package com.tnorm.shared.converters;

import java.time.LocalDate;

import com.fasterxml.jackson.databind.util.StdConverter;

public class LocalDateToNgbDateConverter extends StdConverter<LocalDate, NgbDate> {

	@Override
	public NgbDate convert(LocalDate value) {
		if (value == null) {
			return null;
		}
		NgbDate jsonObject = new NgbDate();
		jsonObject.setYear(value.getYear());
		jsonObject.setMonth(value.getMonthValue());
		jsonObject.setDay(value.getDayOfMonth());
		return jsonObject;
	}
}
