package com.tnorm.shared.converters;

import java.time.LocalDate;

import com.fasterxml.jackson.databind.util.StdConverter;

public class NgbDateToLocalDateConverter extends StdConverter<NgbDate, LocalDate> {

	@Override
	public LocalDate convert(NgbDate value) {
		if (value == null) {
			return null;
		}
		return LocalDate.of(value.getYear(), value.getMonth(), value.getDay());
	}

}
