package com.tnorm.shared.dataaccess;

import org.hibernate.dialect.H2Dialect;
import org.hibernate.dialect.function.StandardSQLFunction;

public class CustomH2Dialect extends H2Dialect {

  public CustomH2Dialect() {
    super();
    registerFunction("MEDIAN", new StandardSQLFunction("MEDIAN"));

  }

}
