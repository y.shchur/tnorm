package com.tnorm.shared.dataaccess.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Version;
import java.io.Serializable;

@Getter
@Setter
public abstract class AbstractEntity implements Serializable {

  protected static final long serialVersionUID = 1L;

  @Version
  private Integer version;
}
