package com.tnorm.shared.util;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.Reader;
import java.util.List;

public class BaseReader {

  private Reader reader;
  private List<CSVRecord> records;
  private int size;

  public BaseReader(Reader reader) throws IOException {
    super();
    this.reader = reader;
    this.loadCsvDatabase();
  }

  private void loadCsvDatabase() throws IOException {
    CSVParser csvParser = new CSVParser(this.reader, CSVFormat.DEFAULT.withQuote(null), 1, 1);
    records = csvParser.getRecords();
    size = records.size();
    csvParser.close();
  }

  public List<CSVRecord> getRecords() {
    return records;
  }
}
