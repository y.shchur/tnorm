package com.tnorm.workspace.dataaccess.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tnorm.workspace.dataaccess.model.WorkspaceEntity;

public interface WorkspaceDao extends JpaRepository<WorkspaceEntity, Long> {

}
