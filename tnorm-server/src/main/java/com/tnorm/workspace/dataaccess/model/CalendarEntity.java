package com.tnorm.workspace.dataaccess.model;

import com.tnorm.shared.config.multitenant.MultiTenantConstants;
import com.tnorm.shared.dataaccess.model.AbstractEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "CALENDAR", schema = MultiTenantConstants.DEFAULT_TENANT_ID)
public class CalendarEntity extends AbstractEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "DESCRIPTION")
  private String description;

  @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE},
      orphanRemoval = true)
  @JoinColumn(name = "CALENDAR_ID")
  private List<CalendarPeriodEntity> intervals = new ArrayList<>();
}
