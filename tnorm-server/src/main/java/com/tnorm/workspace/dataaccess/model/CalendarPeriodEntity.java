package com.tnorm.workspace.dataaccess.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tnorm.shared.config.multitenant.MultiTenantConstants;
import com.tnorm.shared.dataaccess.model.AbstractEntity;
import com.tnorm.workspace.logic.api.to.DailyActivityType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Map;

@Getter
@Setter
@Entity
@Table(name = "CALENDAR_PERIOD", schema = MultiTenantConstants.DEFAULT_TENANT_ID)
public class CalendarPeriodEntity extends AbstractEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "START")
  private LocalDate start;

  @Column(name = "END")
  private LocalDate end;

  @Column(name = "TYPE")
  private DailyActivityType intervalType;

  @JsonProperty("start")
  private void unpackStart(Map<String, Integer> start) {
    this.start = LocalDate.of(start.get("year"), start.get("month"), start.get("day"));
  }

  @JsonProperty("end")
  private void unpackEnd(Map<String, Integer> end) {
    this.end = LocalDate.of(end.get("year"), end.get("month"), end.get("day"));
  }

  @JsonProperty("intervalType")
  private void unpackIntervalType(Map<String, String> intervalType) {
    this.intervalType = DailyActivityType.getAll()
        .stream()
        .filter(e -> e.name().equals(intervalType.get("value")))
        .findAny()
        .orElse(null);
  }
}
