package com.tnorm.workspace.dataaccess.model;

import com.tnorm.scheduling.logic.api.to.TableModelTo;
import com.tnorm.shared.config.multitenant.MultiTenantConstants;
import com.tnorm.shared.dataaccess.model.AbstractEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Getter
@Setter
@Entity
@Table(name = "WORKSPACE", schema = MultiTenantConstants.DEFAULT_TENANT_ID)
public class WorkspaceEntity extends AbstractEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "NAME")
  private String name;

  @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE},
      orphanRemoval = true)
  @JoinColumn(name = "CALENDAR_ID")
  private CalendarEntity calendar;

  @Column(name = "DATA_SOURCE")
  private String dataSource;

  @Transient
  private TableModelTo tableModel;
}
