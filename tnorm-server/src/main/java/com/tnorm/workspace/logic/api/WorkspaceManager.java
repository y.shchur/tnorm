package com.tnorm.workspace.logic.api;

import com.tnorm.workspace.dataaccess.dao.WorkspaceDao;
import com.tnorm.workspace.logic.api.mapper.WorkspaceMapper;
import com.tnorm.workspace.logic.api.to.WorkspaceFullTo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
@RequiredArgsConstructor
public class WorkspaceManager {

  private final WorkspaceDao workspaceDao;
  private final WorkspaceMapper workspaceMapper;

  public List<WorkspaceFullTo> findAllWorkspaces() {
    return workspaceDao.findAll()//
        .stream()
        .map(workspaceMapper::mapWorkspaceEntity2WorkspaceFullTo)
        .collect(Collectors.toList());
  }

  public void saveWorkspace(WorkspaceFullTo workspace) {
    workspaceDao.save(workspaceMapper.mapWorkspaceFullTo2WorkspaceEntity(workspace));
  }
}
