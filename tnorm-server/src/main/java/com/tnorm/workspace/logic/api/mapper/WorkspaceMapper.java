package com.tnorm.workspace.logic.api.mapper;

import org.mapstruct.Mapper;

import com.tnorm.workspace.dataaccess.model.WorkspaceEntity;
import com.tnorm.workspace.logic.api.to.WorkspaceFullTo;

@Mapper(componentModel = "spring")
public interface WorkspaceMapper {

	WorkspaceFullTo mapWorkspaceEntity2WorkspaceFullTo(WorkspaceEntity source);

	WorkspaceEntity mapWorkspaceFullTo2WorkspaceEntity(WorkspaceFullTo source);

}
