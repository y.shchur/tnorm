package com.tnorm.workspace.logic.api.to;

import lombok.Data;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
public class CalendarFullTo {

  private Long id;

  private String description;

  private List<IntervalFullTo> intervals = new ArrayList<>();

  public DayType getDayTypeOf(LocalDate date) {

    return intervals.stream()//
        .filter(interval -> interval.contains(date))
        .map(interval -> DayType.getDayType(date.getDayOfWeek(), interval.getIntervalType()))
        .filter(dayType -> dayType != null)
        .findFirst()
        .orElse(DayType.getDayType(date.getDayOfWeek(), DailyActivityType.NOT_DEFINED));
  }
}
