package com.tnorm.workspace.logic.api.to;

import java.util.EnumSet;

public enum DailyActivityType {

	SCHOOL_YEAR, VACATION, TRADE_ALLOWED, TRADE_PROHIBITED, HOLIDAY, NOT_DEFINED;

	public static EnumSet<DailyActivityType> getAll() {
		return EnumSet.allOf(DailyActivityType.class);
	}

}
