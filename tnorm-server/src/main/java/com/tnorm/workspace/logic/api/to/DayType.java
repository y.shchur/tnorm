package com.tnorm.workspace.logic.api.to;

import java.time.DayOfWeek;
import java.util.EnumSet;

public enum DayType {
	WORKDAY(DayType.workWeek(), DailyActivityType.NOT_DEFINED, 4),

	SATURDAY(DayOfWeek.SATURDAY, DailyActivityType.NOT_DEFINED, 4),

	SUNDAY(DayOfWeek.SUNDAY, DailyActivityType.NOT_DEFINED, 4),

	WORKDAY_SCHOOL(DayType.workWeek(), DailyActivityType.SCHOOL_YEAR, 3),

	WORKDAY_VACATION(DayType.workWeek(), DailyActivityType.VACATION, 2),

	TRADE_SUNDAY(DayOfWeek.SUNDAY, DailyActivityType.TRADE_ALLOWED, 2),

	SUNDAY_WO_TRADE(DayOfWeek.SUNDAY, DailyActivityType.TRADE_PROHIBITED, 2),

	HOLIDAY(DayType.week(), DailyActivityType.HOLIDAY, 1);

	private EnumSet<DayOfWeek> daysOfWeek;

	private DailyActivityType activityType;

	private int priority;

	private DayType(EnumSet<DayOfWeek> daysOfWeek, DailyActivityType activityType, int priority) {
		this.daysOfWeek = daysOfWeek;
		this.activityType = activityType;
		this.priority = priority;
	}

	private DayType(DayOfWeek dayOfWeek, DailyActivityType activityType, int priority) {
		this.daysOfWeek = EnumSet.of(dayOfWeek);
		this.activityType = activityType;
		this.priority = priority;
	}

	public static DayType getDayType(DayOfWeek dayOfWeek, DailyActivityType activityType) {
		DayType result = null;
		for (DayType day : DayType.values()) {
			if (day.daysOfWeek.contains(dayOfWeek) && day.activityType.equals(activityType)
					&& (result == null || result.priority > day.priority)) {
				result = day;
			}
		}
		return result;
	}

	private static EnumSet<DayOfWeek> workWeek() {
		return EnumSet.of(DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY, DayOfWeek.FRIDAY);
	}

	private static EnumSet<DayOfWeek> week() {
		return EnumSet.of(DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY, DayOfWeek.FRIDAY,
				DayOfWeek.SATURDAY, DayOfWeek.SUNDAY);
	}

}
