package com.tnorm.workspace.logic.api.to;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tnorm.shared.converters.LocalDateToNgbDateConverter;
import com.tnorm.shared.converters.NgbDateToLocalDateConverter;
import lombok.Data;

import java.time.LocalDate;
import java.util.Optional;

@Data
public class IntervalFullTo {

  private Long id;

  @JsonSerialize(converter = LocalDateToNgbDateConverter.class)
  @JsonDeserialize(converter = NgbDateToLocalDateConverter.class)
  private LocalDate start;

  @JsonSerialize(converter = LocalDateToNgbDateConverter.class)
  @JsonDeserialize(converter = NgbDateToLocalDateConverter.class)
  private LocalDate end;

  private DailyActivityType intervalType;

  public boolean contains(LocalDate date) {
    if (date.isAfter(start) && date.isBefore(end) || date.isEqual(start) || date.isEqual(end)) {
      return true;
    }
    return false;
  }

  public Optional<DailyActivityType> getDayTypeOf(LocalDate date) {
    if (this.contains(date)) {
      return Optional.of(intervalType);
    } else {
      return Optional.ofNullable(intervalType);
    }
  }

}
