package com.tnorm.workspace.logic.api.to;

import com.tnorm.scheduling.logic.api.to.TableModelTo;
import lombok.Data;

@Data
public class WorkspaceFullTo {

  private Long id;

  private String name;

  private CalendarFullTo calendar;

  private String dataSource;

  private TableModelTo tableModel;
}
