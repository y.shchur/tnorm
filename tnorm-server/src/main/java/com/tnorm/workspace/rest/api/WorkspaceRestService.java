package com.tnorm.workspace.rest.api;

import com.tnorm.workspace.logic.api.WorkspaceManager;
import com.tnorm.workspace.logic.api.to.WorkspaceFullTo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class WorkspaceRestService {

  private final WorkspaceManager workspaceManager;

  @GetMapping(WorkspaceUrlConstants.ALL_WORKSPACES)
  public List<WorkspaceFullTo> getWorkspaces() {
    return workspaceManager.findAllWorkspaces();
  }

  @PostMapping(WorkspaceUrlConstants.API_WORKSPACE)
  public void saveWorkspace(WorkspaceFullTo project) {
    workspaceManager.saveWorkspace(project);
  }

}
