package com.tnorm.workspace.rest.api;

public interface WorkspaceUrlConstants {
  String API_WORKSPACE = "/api/workspace";
  String ALL_WORKSPACES = API_WORKSPACE + "/workspaces";
}
