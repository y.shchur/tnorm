create table TEST_PROJECT_SCHEMA.STOP_POINT(
	ID				BIGINT NOT NULL PRIMARY KEY,
	NAME			VARCHAR2(40 CHAR),
	VERSION			INT
);