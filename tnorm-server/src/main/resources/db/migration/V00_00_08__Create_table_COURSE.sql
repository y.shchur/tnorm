create table TEST_PROJECT_SCHEMA.COURSE(
    ID                  BIGINT IDENTITY NOT NULL,
    LINE                VARCHAR2(20 CHAR),
    VERSION             INT
);