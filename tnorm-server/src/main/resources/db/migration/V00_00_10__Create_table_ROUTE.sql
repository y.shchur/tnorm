create table TEST_PROJECT_SCHEMA.ROUTE(
    ID                  BIGINT IDENTITY NOT NULL,
    LINE_NO             VARCHAR2(20 CHAR),
    DESCRIPTION         VARCHAR2(20 CHAR),
    VERSION             INT
);