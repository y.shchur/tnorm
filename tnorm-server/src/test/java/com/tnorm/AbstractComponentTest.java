package com.tnorm;

import com.tnorm.shared.config.multitenant.MultiTenantConstants;
import com.tnorm.shared.config.multitenant.TenantContext;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Environment;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.hibernate.jpa.AvailableSettings;

import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource("/application-test.properties")
public abstract class AbstractComponentTest {

  @Before
  public void setTenant(){
    TenantContext.setCurrentTenant("TEST_PROJECT_SCHEMA");
  }

//	@Mock
//	private CurrentTenantIdentifierResolver currentTenantIdentifierResolver;
//	
//	@Autowired
//	private JpaProperties jpaProperties;
//
//	private SessionFactory sessionFactory;
//
//	@Before
//	public void setup() throws IOException {
//		MockitoAnnotations.initMocks(this);
//
//		when(currentTenantIdentifierResolver.validateExistingCurrentSessions()).thenReturn(false);
//
//		Map<String, Object> hibernateProps = new LinkedHashMap<>();
//		hibernateProps.putAll(this.jpaProperties.getProperties());
//		hibernateProps.put(Environment.MULTI_TENANT_IDENTIFIER_RESOLVER, currentTenantIdentifierResolver);
//
//		sessionFactory = buildSessionFactory(hibernateProps);
//
//		initTenant(TenantIdNames.MYDB1);
//		initTenant(TenantIdNames.MYDB2);
//	}
//
//	protected void initTenant(String tenantId) {
//		when(currentTenantIdentifierResolver.resolveCurrentTenantIdentifier()).thenReturn(tenantId);
//		createCarTable();
//	}

}
