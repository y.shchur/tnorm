package com.tnorm.scheduling.dataaccess.dao;

import com.tnorm.AbstractComponentTest;
import com.tnorm.scheduling.dataaccess.model.LinkEntity;
import com.tnorm.scheduling.logic.api.to.MedianCalculationCriteria;
import com.tnorm.workspace.logic.api.to.DayType;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Duration;
import java.time.LocalTime;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class TimeSnapRepositoryTest extends AbstractComponentTest {

  @Autowired
  private TimeSnapCustomRepositoryImpl timeSnapRepo;

  @Test
  public void shouldFindMedian() {
    // given
    MedianCalculationCriteria criteria = new MedianCalculationCriteria.Builder()//
        .dayType(DayType.WORKDAY_SCHOOL)
        .sinceTime(LocalTime.of(7, 0))
        .tillTime(LocalTime.of(8, 0))
        .build();
    // when
    Map<LinkEntity, Duration> links = timeSnapRepo.getMedian(criteria);

    // then
    assertEquals(341, links.size());
  }
}
