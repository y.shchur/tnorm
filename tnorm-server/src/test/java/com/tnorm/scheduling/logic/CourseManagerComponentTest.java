package com.tnorm.scheduling.logic;

import com.tnorm.AbstractComponentTest;
import com.tnorm.scheduling.dataaccess.dao.StopPointRepository;
import com.tnorm.scheduling.dataaccess.model.CourseEntity;
import com.tnorm.scheduling.dataaccess.model.StopPointEntity;
import com.tnorm.scheduling.dataaccess.model.TimeSnapEntity;
import com.tnorm.scheduling.logic.api.CourseManager;
import com.tnorm.shared.testdata.CourseTestData;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class CourseManagerComponentTest extends AbstractComponentTest {

  @Autowired
  private CourseManager courseManager;

  @Autowired
  private StopPointRepository stopPointRepository;

  @Test
  public void shouldSaveCourseToDB() {
    //given
    CourseEntity course = CourseTestData.validCourse1A();
    List<StopPointEntity> stopPointList = course.getTimeSnaps().stream().map(TimeSnapEntity::getStopPoint).collect(Collectors.toList());
    int timeSnapAmount = course.getTimeSnaps().size();

    //when
    CourseEntity savedCourse = courseManager.save(course);

    //then
    assertThat(savedCourse).isNotNull();
    assertThat(savedCourse.getTimeSnaps()).hasSize(timeSnapAmount);
  }

}
