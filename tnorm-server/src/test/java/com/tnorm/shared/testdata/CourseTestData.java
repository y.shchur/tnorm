package com.tnorm.shared.testdata;

import com.tnorm.scheduling.dataaccess.model.CourseEntity;
import org.assertj.core.util.Lists;

public class CourseTestData {

  public static final String LINE_NO_1_A = "1A";
  public static final String LINE_NO_1_B = "1B";

  public static CourseEntity validCourse1A() {
    CourseEntity course = new CourseEntity();
    course.setLine(LINE_NO_1_A);
    course.setTimeSnaps(Lists.newArrayList(
        TimeSnapTestData.validTimeSnap0(),
        TimeSnapTestData.validTimeSnap1(),
        TimeSnapTestData.validTimeSnap2(),
        TimeSnapTestData.validTimeSnap3()));
    return course;
  }

  public static CourseEntity validCourse1B() {
    CourseEntity course = new CourseEntity();
    course.setLine(LINE_NO_1_B);
    course.setTimeSnaps(Lists.newArrayList(
        TimeSnapTestData.validTimeSnap4(),
        TimeSnapTestData.validTimeSnap5(),
        TimeSnapTestData.validTimeSnap6(),
        TimeSnapTestData.validTimeSnap7()));
    return course;
  }
}
