package com.tnorm.shared.testdata;

import com.tnorm.scheduling.dataaccess.model.LinkEntity;
import com.tnorm.scheduling.dataaccess.model.LinkId;
import com.tnorm.scheduling.dataaccess.model.StopPointEntity;

public class LinkTestData {

  public static LinkEntity validLink1_2() {
    StopPointEntity start = StopPointTestData.validStop1();
    StopPointEntity end = StopPointTestData.validStop2();

    return new LinkEntity(
        new LinkId(start.getId(), end.getId()),
        start,
        end);
  }

  public static LinkEntity validLink2_3() {
    StopPointEntity start = StopPointTestData.validStop2();
    StopPointEntity end = StopPointTestData.validStop3();

    return new LinkEntity(
        new LinkId(start.getId(), end.getId()),
        start,
        end);
  }

  public static LinkEntity validLink3_4() {
    StopPointEntity start = StopPointTestData.validStop3();
    StopPointEntity end = StopPointTestData.validStop4();

    return new LinkEntity(
        new LinkId(start.getId(), end.getId()),
        start,
        end);
  }

  public static LinkEntity validLink4_3() {
    StopPointEntity start = StopPointTestData.validStop4();
    StopPointEntity end = StopPointTestData.validStop3();

    return new LinkEntity(
        new LinkId(start.getId(), end.getId()),
        start,
        end);
  }

  public static LinkEntity validLink3_2() {
    StopPointEntity start = StopPointTestData.validStop3();
    StopPointEntity end = StopPointTestData.validStop2();

    return new LinkEntity(
        new LinkId(start.getId(), end.getId()),
        start,
        end);
  }

  public static LinkEntity validLink2_1() {
    StopPointEntity start = StopPointTestData.validStop2();
    StopPointEntity end = StopPointTestData.validStop1();

    return new LinkEntity(
        new LinkId(start.getId(), end.getId()),
        start,
        end);
  }
}
