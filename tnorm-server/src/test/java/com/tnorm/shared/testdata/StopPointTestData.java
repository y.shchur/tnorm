package com.tnorm.shared.testdata;

import com.tnorm.scheduling.dataaccess.model.StopPointEntity;

public class StopPointTestData {

  public static final Long STOP_POINT_ID_1 = 100L;
  public static final Long STOP_POINT_ID_2 = 101L;
  public static final Long STOP_POINT_ID_3 = 102L;
  public static final Long STOP_POINT_ID_4 = 103L;
  public static final Long STOP_POINT_ID_5 = 104L;
  public static final Long STOP_POINT_ID_6 = 105L;

  public static final String STOP_POINT_NAME_1 = "1 Maja";
  public static final String STOP_POINT_NAME_2 = "Sienkiewicza";
  public static final String STOP_POINT_NAME_3 = "Moniuszki";
  public static final String STOP_POINT_NAME_4 = "Kotlarska";
  public static final String STOP_POINT_NAME_5 = "Rynek";
  public static final String STOP_POINT_NAME_6 = "Dworzec";

  public static StopPointEntity validStop1(){
    StopPointEntity stopPoint = new StopPointEntity();
    stopPoint.setId(STOP_POINT_ID_1);
    stopPoint.setName(STOP_POINT_NAME_1);
    return stopPoint;
  }

  public static StopPointEntity validStop2(){
    StopPointEntity stopPoint = new StopPointEntity();
    stopPoint.setId(STOP_POINT_ID_2);
    stopPoint.setName(STOP_POINT_NAME_2);
    return stopPoint;
  }

  public static StopPointEntity validStop3(){
    StopPointEntity stopPoint = new StopPointEntity();
    stopPoint.setId(STOP_POINT_ID_3);
    stopPoint.setName(STOP_POINT_NAME_3);
    return stopPoint;
  }

  public static StopPointEntity validStop4(){
    StopPointEntity stopPoint = new StopPointEntity();
    stopPoint.setId(STOP_POINT_ID_4);
    stopPoint.setName(STOP_POINT_NAME_4);
    return stopPoint;
  }
}
