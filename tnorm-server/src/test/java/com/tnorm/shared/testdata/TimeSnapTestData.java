package com.tnorm.shared.testdata;

import com.tnorm.scheduling.dataaccess.model.TimeSnapEntity;

public class TimeSnapTestData {

  public static TimeSnapEntity validTimeSnap0() {
    TimeSnapEntity timeSnap = new TimeSnapEntity();
    timeSnap.setStopPoint(StopPointTestData.validStop1());
    timeSnap.setLink(null);
    return timeSnap;
  }

  public static TimeSnapEntity validTimeSnap1() {
    TimeSnapEntity timeSnap = new TimeSnapEntity();
    timeSnap.setStopPoint(StopPointTestData.validStop2());
    timeSnap.setLink(LinkTestData.validLink1_2());
    return timeSnap;
  }

  public static TimeSnapEntity validTimeSnap2() {
    TimeSnapEntity timeSnap = new TimeSnapEntity();
    timeSnap.setStopPoint(StopPointTestData.validStop3());
    timeSnap.setLink(LinkTestData.validLink2_3());
    return timeSnap;
  }

  public static TimeSnapEntity validTimeSnap3() {
    TimeSnapEntity timeSnap = new TimeSnapEntity();
    timeSnap.setStopPoint(StopPointTestData.validStop4());
    timeSnap.setLink(LinkTestData.validLink3_4());
    return timeSnap;
  }

  public static TimeSnapEntity validTimeSnap4() {
    TimeSnapEntity timeSnap = new TimeSnapEntity();
    timeSnap.setStopPoint(StopPointTestData.validStop4());
    timeSnap.setLink(null);
    return timeSnap;
  }

  public static TimeSnapEntity validTimeSnap5() {
    TimeSnapEntity timeSnap = new TimeSnapEntity();
    timeSnap.setStopPoint(StopPointTestData.validStop3());
    timeSnap.setLink(LinkTestData.validLink4_3());
    return timeSnap;
  }

  public static TimeSnapEntity validTimeSnap6() {
    TimeSnapEntity timeSnap = new TimeSnapEntity();
    timeSnap.setStopPoint(StopPointTestData.validStop2());
    timeSnap.setLink(LinkTestData.validLink3_2());
    return timeSnap;
  }

  public static TimeSnapEntity validTimeSnap7() {
    TimeSnapEntity timeSnap = new TimeSnapEntity();
    timeSnap.setStopPoint(StopPointTestData.validStop1());
    timeSnap.setLink(LinkTestData.validLink2_1());
    return timeSnap;
  }
}
