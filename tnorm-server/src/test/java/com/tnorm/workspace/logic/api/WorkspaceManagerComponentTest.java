package com.tnorm.workspace.logic.api;

import com.google.common.collect.Lists;
import com.tnorm.AbstractComponentTest;
import com.tnorm.shared.config.multitenant.MultiTenantConstants;
import com.tnorm.shared.config.multitenant.TenantContext;
import com.tnorm.workspace.dataaccess.dao.WorkspaceDao;
import com.tnorm.workspace.logic.api.to.CalendarFullTo;
import com.tnorm.workspace.logic.api.to.IntervalFullTo;
import com.tnorm.workspace.logic.api.to.WorkspaceFullTo;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

public class WorkspaceManagerComponentTest extends AbstractComponentTest {

  @Autowired
  private WorkspaceManager workspaceManager;

  @Autowired
  private WorkspaceDao workspaceDao;

  @Test
  public void shouldSaveWorkspaceInDB() {
    // given
    TenantContext.setCurrentTenant(MultiTenantConstants.DEFAULT_TENANT_ID);
    WorkspaceFullTo workspaceTo = new WorkspaceFullTo();
    CalendarFullTo calendarTo = new CalendarFullTo();
    IntervalFullTo periodTo = new IntervalFullTo();
    calendarTo.setIntervals(Lists.newArrayList(periodTo));
    workspaceTo.setCalendar(calendarTo);
    Long existingNumber = workspaceDao.count();

    // when
    workspaceManager.saveWorkspace(workspaceTo);

    // then
    assertThat(workspaceDao.count()).isEqualTo(existingNumber + 1);
  }

}
