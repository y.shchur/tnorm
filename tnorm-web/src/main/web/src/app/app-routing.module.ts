import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProjectDashboardComponent} from './workspace/project-dashboard/project-dashboard.component';
import {WorkspaceComponent} from './workspace/workspace.component';
import {RouteOverviewComponent} from './scheduling/route/route-overview/route-overview.component';
import {RouteCreationComponent} from './scheduling/route/route-creation/route-creation.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'dashboard'},
  {path: 'dashboard', component: ProjectDashboardComponent},
  {
    path: 'workspace', component: WorkspaceComponent, children: [
      {path: 'routes', component: RouteOverviewComponent},
      {path: 'routes/new', component: RouteCreationComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule {
}
