import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToolbarComponent} from './shared/component/toolbar/toolbar.component';
import {MaterialModule} from '../material-module';
import {ProjectDashboardComponent} from './workspace/project-dashboard/project-dashboard.component';
import {AppRoutingModule} from './/app-routing.module';
import {NewProjectDialogComponent} from './workspace/new-project-dialog/new-project-dialog.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ProjectTileComponent} from './workspace/project-tile/project-tile.component';
import {HttpClientModule} from '@angular/common/http';
import {WorkspaceComponent} from './workspace/workspace.component';
import {PapaParseModule} from 'ngx-papaparse';
import {CalendarDialogComponent} from './workspace/calendar-dialog/calendar-dialog.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbDatePipe} from './shared/pipe/ngb-date.pipe';
import {SchedulingModule} from './scheduling/scheduling.module';

@NgModule({
  imports: [
    SchedulingModule,
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    PapaParseModule,
    NgbModule
  ],
  declarations: [
    AppComponent,
    ToolbarComponent,
    ProjectDashboardComponent,
    NewProjectDialogComponent,
    ProjectTileComponent,
    WorkspaceComponent,
    CalendarDialogComponent,
    NgbDatePipe
  ],
  entryComponents: [NewProjectDialogComponent, CalendarDialogComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
