import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HeaderDataBinderComponent} from './header-data-binder.component';

describe('HeaderDataBinderComponent', () => {
  let component: HeaderDataBinderComponent;
  let fixture: ComponentFixture<HeaderDataBinderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderDataBinderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderDataBinderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
