import {TableModel} from '../../../shared/model/table-model';
import {ImportDataService} from '../../import-data.service';
import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-header-data-binder',
  templateUrl: './header-data-binder.component.html',
  styleUrls: ['./header-data-binder.component.css']
})
export class HeaderDataBinderComponent implements OnInit {
  @Input()
  columnName: string;
  tableModelProperties: string[];
  selectedProperty: string;
  tableModel: TableModel;

  constructor(public workspaceService: ImportDataService) {
    workspaceService._tableModel.subscribe(t => {
      this.tableModel = t;
      this.generateFieldNames(t);
    });
  }

  ngOnInit() {
    this.tableModel = new TableModel();
    this.generateFieldNames(this.tableModel);
  }

  onColumnSelectionChange($event) {
    if (this.selectedProperty || !$event.value) {
      this.tableModel[this.selectedProperty] = null;
    }
    if ($event.value && $event.value !== 'null') {
      this.selectedProperty = $event.value;
      this.tableModel[$event.value] = this.columnName;
      this.updateTableModel();
    }
  }

  setCourseId($event) {
    if ($event.checked) {
      this.tableModel.courseIdentityCols.push(this.columnName);
    } else {
      this.tableModel.courseIdentityCols = this.tableModel.courseIdentityCols.filter(
        e => e !== this.columnName
      );
    }
    this.updateTableModel();
  }

  setDestinationStopFlagValue($event) {
    this.tableModel.destinationStopFlagValue = $event.target.value;
    this.updateTableModel();
  }

  private updateTableModel() {
    this.workspaceService._tableModel.next(this.tableModel);
  }

  generateFieldNames(tableModel: TableModel) {
    this.tableModelProperties = Object.getOwnPropertyNames(tableModel).filter(
      fieldName =>
        tableModel[fieldName] === null &&
        fieldName !== 'destinationStopFlagValue' &&
        fieldName !== 'courseIdentityCols'
    );
  }
}
