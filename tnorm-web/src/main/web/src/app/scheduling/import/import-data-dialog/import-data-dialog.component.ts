import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {Papa} from 'ngx-papaparse';
import {CharsetConstants} from 'src/app/shared/charsets-constants';
import {ImportDataService} from '../import-data.service';

@Component({
  selector: 'app-import-data-dialog',
  templateUrl: './import-data-dialog.component.html',
  styleUrls: ['./import-data-dialog.component.css']
})
export class ImportDataDialogComponent implements OnInit {
  private file: File;
  fileEncoding: string;
  allEncodings: string[];
  dataList: Object[];
  selectedTableModelProperties: string[];
  displayedColumns: string[];

  constructor(
    public dialogRef: MatDialogRef<ImportDataDialogComponent>,
    private workspaceService: ImportDataService,
    private parser: Papa
  ) {
    this.allEncodings = CharsetConstants.ALL.map(e => e.name);
  }

  onFileInput(event) {
    this.file = event.target.files[0];
    this.parseFile();
  }

  parseFile() {
    console.log(this.fileEncoding);
    this.parser.parse(this.file, {
      header: true,
      skipEmptyLines: true,
      preview: 10,
      encoding: this.fileEncoding,
      complete: result => {
        console.log(result);
        this.dataList = result.data;
      }
    });
  }

  importData() {
    this.workspaceService.importCsvData(this.file, this.fileEncoding);
  }

  ngOnInit() {}
  onNoClick(): void {
    this.dialogRef.close();
  }
}
