import {TableModel} from 'src/app/scheduling/shared/model/table-model';
import {Injectable} from '@angular/core';
import {CurrentWorkspaceService} from '../../workspace/current-workspace.service';
import {Subject} from 'rxjs';
import {TableImportTo} from '../../workspace/shared/model/data-import-to';
import {SchedulingRestService} from '../shared/rest/scheduling-rest.service';

@Injectable({
  providedIn: 'root'
})
export class ImportDataService {
  _tableModel: Subject<TableModel>;
  tableModel: TableModel;

  constructor(
    private projectService: CurrentWorkspaceService,
    private schedulingRestService: SchedulingRestService
  ) {
    this.initTableModelSubject();
  }

  public importCsvData(file: File, fileEncoding: string) {
    const table = new TableImportTo(file, fileEncoding, this.tableModel);
    this.schedulingRestService.sendCsvData(table);
    this.initTableModelSubject();
  }

  private initTableModelSubject() {
    this._tableModel = new Subject<TableModel>();
    this._tableModel.subscribe(t => {
      this.tableModel = t;
      console.log(t);
    });
  }
}
