import {Component, OnDestroy, OnInit} from '@angular/core';
import {RouteTo} from '../../shared/model/route-to';
import {LinkFullTo} from '../../shared/model/link-full-to';
import {Observable, Subject} from 'rxjs';
import {SchedulingRestService} from '../../shared/rest/scheduling-rest.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RouteSavingTo} from '../../shared/model/route-saving-to';
import {LinkTo} from '../../shared/model/link-to';
import {takeUntil} from 'rxjs/operators';
import {TnSnackbarService} from '../../../shared/service/tn-snackbar.service';

@Component({
  selector: 'tn-route-creation',
  templateUrl: './route-creation.component.html',
  styleUrls: ['./route-creation.component.css']
})
export class RouteCreationComponent implements OnInit, OnDestroy {

  private componentDestroy$ = new Subject();

  route: RouteTo;

  latestSavedRoute: RouteTo;

  routeFormGroup: FormGroup;

  nextPossibleLinks$: Observable<LinkFullTo[]>;

  constructor(private restService: SchedulingRestService,
              private formBuilder: FormBuilder,
              private snackbarService: TnSnackbarService) {
    this.route = new RouteTo();
    this.routeFormGroup = this.createFormGroup();
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.componentDestroy$.next();
    this.componentDestroy$.complete();
  }

  createFormGroup(): FormGroup {
    return this.formBuilder.group(
      {
        lineNo: [this.route.lineNo, Validators.required],
        description: [this.route.description],
      });
  }

  findNextPossibleLinks() {
    if (!this.route.links || this.route.links.length === 0) {
      this.nextPossibleLinks$ = this.restService.findAllLinks();
    } else {
      const lastStopPointId = this.route.links[this.route.links.length - 1].end.id;
      this.nextPossibleLinks$ = this.restService.findNextLinks(lastStopPointId);
    }
  }

  addToRoute($event) {
    this.route.links.push($event);
    this.findNextPossibleLinks();
  }

  deleteFromRoute($event) {
    this.route.links.splice(this.route.links.lastIndexOf($event));
  }

  saveRoute() {
    const routeToSave = new RouteSavingTo();
    routeToSave.id = this.route.id;
    routeToSave.lineNo = this.routeFormGroup.value.lineNo;
    routeToSave.description = this.routeFormGroup.value.description;
    routeToSave.links = this.route.links.map<LinkTo>(link => new LinkTo(link.start.id, link.end.id));

    this.restService.saveRoute(routeToSave)
      .pipe(takeUntil(this.componentDestroy$))
      .subscribe(savedRoute => {
        this.snackbarService.showInfo('Route ' + savedRoute.lineNo + ' was successfully saved');
        this.latestSavedRoute = savedRoute;
      });
  }

}
