import {Component, OnInit} from '@angular/core';
import {LinkTo} from '../../shared/model/link-to';
import {RouteTo} from '../../shared/model/route-to';
import {Observable} from 'rxjs';
import {SchedulingRestService} from '../../shared/rest/scheduling-rest.service';

@Component({
  selector: 'tn-route-overview',
  templateUrl: './route-overview.component.html',
  styleUrls: ['./route-overview.component.css']
})
export class RouteOverviewComponent implements OnInit {


  linesWithRoutes = new Map<string, RouteTo[]>();
  routes$: Observable<RouteTo[]>;
  columnsToDisplay = ['lineNo', 'routes'];
  expandedElement: LinkTo | null;


  constructor(private restService: SchedulingRestService) {
    restService.findAllRoutes().subscribe(routes => this.groupByLine(routes));
  }

  ngOnInit() {
  }

  getLines(): string[] {
    return Array.from(this.linesWithRoutes.keys());
  }

  private groupByLine(routes: RouteTo[]) {
    routes.forEach(route => {
      if (this.linesWithRoutes.has(route.lineNo)) {
        this.linesWithRoutes.get(route.lineNo).push(route);
      } else {
        this.linesWithRoutes.set(route.lineNo, [route])
      }
    });
  }

}
