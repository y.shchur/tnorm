import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouteOverviewComponent} from './route/route-overview/route-overview.component';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from '../../material-module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from '../app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {PapaParseModule} from 'ngx-papaparse';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouteCreationComponent} from './route/route-creation/route-creation.component';
import {LinkRendererComponent} from './shared/renderer/link-renderer/link-renderer.component';
import {ImportDataDialogComponent} from './import/import-data-dialog/import-data-dialog.component';
import {HeaderDataBinderComponent} from './import/import-data-dialog/header-data-binder/header-data-binder.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    PapaParseModule,
    NgbModule
  ],
  exports: [
    RouteOverviewComponent,
    RouteCreationComponent,
    ImportDataDialogComponent,
    HeaderDataBinderComponent
  ],
  declarations: [RouteOverviewComponent,
    RouteCreationComponent,
    ImportDataDialogComponent,
    LinkRendererComponent,
    HeaderDataBinderComponent],
  entryComponents: [ImportDataDialogComponent]
})
export class SchedulingModule {
}
