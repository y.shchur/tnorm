import {StopPointTo} from './stop-point-to';

export class LinkFullTo {
  start: StopPointTo;
  end: StopPointTo;
}
