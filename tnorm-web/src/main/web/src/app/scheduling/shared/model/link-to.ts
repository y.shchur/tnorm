export class LinkTo {
  startId: number;
  endId: number;

  constructor(startId: number, endId: number) {
    this.startId = startId;
    this.endId = endId;
  }
}
