import {LinkTo} from './link-to';

export class RouteSavingTo {
  id: number;
  lineNo: string;
  description: string;
  links: LinkTo[] = [];
}
