import {LinkFullTo} from './link-full-to';

export class RouteTo {
  id: number;
  lineNo: string;
  description: string;
  links: LinkFullTo[] = [];
}
