export class TableModel {
  dateCol: string;
  serviceCol: string;
  lineNoCol: string;
  stopNameCol: string;
  stopPointIdCol: string;
  destinationStopFlagCol: string;
  destinationStopFlagValue: string;
  scheduledTimeCol: string;
  snapTimeCol: string;
  courseIdentityCols: string[];

  constructor() {
    this.dateCol = null;
    this.serviceCol = null;
    this.lineNoCol = null;
    this.stopNameCol = null;
    this.stopPointIdCol = null;
    this.destinationStopFlagCol = null;
    this.destinationStopFlagValue = '';
    this.scheduledTimeCol = null;
    this.snapTimeCol = null;
    this.courseIdentityCols = [];
  }
}
