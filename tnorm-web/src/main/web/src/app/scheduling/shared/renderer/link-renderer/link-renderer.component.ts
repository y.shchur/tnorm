import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LinkFullTo} from '../../model/link-full-to';

@Component({
  selector: 'tn-link-renderer',
  templateUrl: './link-renderer.component.html',
  styleUrls: ['./link-renderer.component.css']
})
export class LinkRendererComponent implements OnInit {

  @Input()
  link: LinkFullTo;

  @Input()
  lastElement: boolean = false;

  @Input()
  linkSuggestion: boolean = false;

  leftOffset: number = 0;

  @Output()
  add: EventEmitter<LinkFullTo> = new EventEmitter<LinkFullTo>();

  @Output()
  delete: EventEmitter<LinkFullTo> = new EventEmitter<LinkFullTo>();

  constructor() {
  }

  ngOnInit() {
    this.getLeftOffset();
  }

  getLeftOffset() {
    return this.linkSuggestion ? 30 : 0;
  }

  emitAddEvent() {
    this.add.next(this.link);
  }

  emitDeleteEvent() {
    this.delete.next(this.link);
  }
}
