import {Injectable} from '@angular/core';
import {CurrentWorkspaceService} from '../../../workspace/current-workspace.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {RouteTo} from '../model/route-to';
import {LinkFullTo} from '../model/link-full-to';
import {RouteSavingTo} from '../model/route-saving-to';
import {TableImportTo} from '../../../workspace/shared/model/data-import-to';
import {SchedulingUrlConstants} from './scheduling-url-constants';
import {AbstractRestService} from '../../../shared/service/abstract-rest-service';

@Injectable({
  providedIn: 'root'
})
export class SchedulingRestService extends AbstractRestService {

  constructor(readonly projectService: CurrentWorkspaceService, readonly http: HttpClient) {
    super(projectService);
  }

  public sendCsvData(table: TableImportTo) {
    console.log(table);
    const formData: FormData = new FormData();
    formData.append('encoding', table.encoding);
    formData.append('tableModel', JSON.stringify(table.tableModel));
    formData.append('file', table.file);
    console.log(this.httpOptions);
    this.http
      .post<TableImportTo>(
        SchedulingUrlConstants.IMPORT_CSV,
        formData,
        this.httpOptions
      )
      .subscribe();
  }

  findAllRoutes(): Observable<RouteTo[]> {
    return this.http.get<RouteTo[]>(SchedulingUrlConstants.ALL_ROUTES, this.httpOptions);
  }

  findAllLinks(): Observable<LinkFullTo[]> {
    return this.http.get<LinkFullTo[]>(SchedulingUrlConstants.ALL_LINKS, this.httpOptions);
  }

  findNextLinks(startStopPointId: number): Observable<LinkFullTo[]> {
    return this.http.get<LinkFullTo[]>(SchedulingUrlConstants.NEXT_LINKS + `/${startStopPointId}`, this.httpOptions);
  }

  saveRoute(route: RouteSavingTo): Observable<RouteTo> {
    return this.http.post<RouteTo>(SchedulingUrlConstants.SAVE_ROUTE, route, this.httpOptions);
  }
}
