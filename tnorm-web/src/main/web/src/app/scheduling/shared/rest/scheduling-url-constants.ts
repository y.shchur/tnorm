export class SchedulingUrlConstants {
  static readonly API_SCHEDULING: string = '/api/scheduling';
  static readonly IMPORT_CSV: string = SchedulingUrlConstants.API_SCHEDULING + '/import';
  static readonly ALL_ROUTES: string = SchedulingUrlConstants.API_SCHEDULING + '/routes';
  static readonly SAVE_ROUTE: string = SchedulingUrlConstants.API_SCHEDULING + '/routes/save';
  static readonly ALL_LINKS: string = SchedulingUrlConstants.API_SCHEDULING + '/links';
  static readonly NEXT_LINKS: string = SchedulingUrlConstants.API_SCHEDULING + '/next_link';
}
