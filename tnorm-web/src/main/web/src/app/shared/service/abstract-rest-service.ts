import {HttpHeaders} from '@angular/common/http';
import {CurrentWorkspaceService} from '../../workspace/current-workspace.service';

export class AbstractRestService {
  protected httpOptions = {
    headers: new HttpHeaders({
      projectSourceID: ''
    })
  };

  constructor(readonly projectService: CurrentWorkspaceService) {
    this.setProjectSource();
  }

  private setProjectSource() {
    this.projectService.currentProject$.subscribe(p => {
      this.httpOptions.headers = this.httpOptions.headers.set('projectSourceID', p.dataSource);
    });
  }
}
