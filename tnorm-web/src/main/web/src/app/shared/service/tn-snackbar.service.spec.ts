import {inject, TestBed} from '@angular/core/testing';

import {TnSnackbarService} from './tn-snackbar.service';

describe('TnSnackbarService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TnSnackbarService]
    });
  });

  it('should be created', inject([TnSnackbarService], (service: TnSnackbarService) => {
    expect(service).toBeTruthy();
  }));
});
