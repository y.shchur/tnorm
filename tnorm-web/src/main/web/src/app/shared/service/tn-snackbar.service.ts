import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class TnSnackbarService {

  constructor(private snackbar: MatSnackBar) {
  }

  showInfo(message: string) {
    this.snackbar.open(message, null, {duration: 2000});
  }
}
