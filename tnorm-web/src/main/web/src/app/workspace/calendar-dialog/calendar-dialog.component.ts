import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {NgbDate} from '@ng-bootstrap/ng-bootstrap';
import {Subscription} from 'rxjs';
import {CurrentWorkspaceService} from 'src/app/workspace/current-workspace.service';
import {Calendar} from 'src/app/workspace/shared/model/calendar';
import {DailyActivityType} from 'src/app/workspace/shared/model/daily-activity-type';
import {Interval} from 'src/app/workspace/shared/model/interval';
import {Project} from 'src/app/workspace/shared/model/project';
import {ImportDataDialogComponent} from '../../scheduling/import/import-data-dialog/import-data-dialog.component';
import {DailyActivityTypeExpression} from '../shared/model/daily-activity-type-map';
import {WorkspaceRestService} from '../shared/rest/workspace-rest.service';

@Component({
  selector: 'app-calendar-dialog',
  templateUrl: './calendar-dialog.component.html',
  styleUrls: ['./calendar-dialog.component.css']
})
export class CalendarDialogComponent implements OnInit, OnDestroy {
  project: Project;
  calendar: Calendar;
  dailyActivityTypes: DailyActivityTypeExpression[];
  fromDate: NgbDate;
  toDate: NgbDate;
  hoveredDate: NgbDate;
  selectedDayType: DailyActivityTypeExpression;

  private projectServiceSubscription: Subscription;

  constructor(public dialogRef: MatDialogRef<ImportDataDialogComponent>,
              private workspaceRestService: WorkspaceRestService,
              private currentWorkspaceService: CurrentWorkspaceService) {
    this.projectServiceSubscription = this.currentWorkspaceService.currentProject$.subscribe(p => (this.project = p));
    if (this.project.calendar) {
      this.calendar = this.project.calendar;
    } else {
      this.calendar = new Calendar();
    }
    this.dailyActivityTypes = DailyActivityTypeExpression.AllValues;
    console.log(this.dailyActivityTypes);
  }

  addIntervalToCalendar(value) {
    this.selectedDayType = value;
    this.calendar.intervals.push(new Interval(this.fromDate, this.toDate, value.value));
  }

  removeIntervalFromCalendar(interval: Interval) {
    console.log(interval);
    this.calendar.intervals = this.calendar.intervals.filter(i => i !== interval);
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && (date.after(this.fromDate) || date.equals(this.fromDate))) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }
  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }

  getTypeRangeClasses(date: NgbDate): string[] {
    return this.calendar.intervals
      .filter(i => Interval.checkDateIsInInterval(date, i))
      .map(i => this.getStyleOfDailyActivityType(i.intervalType));
  }

  getStyleOfDailyActivityType(dailyActivityType: DailyActivityType): string {
    const rez = DailyActivityTypeExpression.DailyActivityTypeMap.get(dailyActivityType).style;
    console.log('--' + rez);
    return rez;
  }

  getStartDate(offset: number) {
    return { year: 2019, month: 1 + offset };
  }
  saveCalendar() {
    this.project.calendar = this.calendar;
    this.currentWorkspaceService.currentProject$.next(this.project);
    this.workspaceRestService.saveCurrentWorkspace().subscribe(i => console.log(i));
  }
  ngOnInit() {}
  ngOnDestroy(): void {
    this.projectServiceSubscription.unsubscribe();
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
