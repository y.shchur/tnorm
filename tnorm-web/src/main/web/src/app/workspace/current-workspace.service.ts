import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Project} from './shared/model/project';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CurrentWorkspaceService {
  public currentProject$: BehaviorSubject<Project>;

  constructor(readonly http: HttpClient) {
    this.currentProject$ = new BehaviorSubject<Project>(null);
  }
}
