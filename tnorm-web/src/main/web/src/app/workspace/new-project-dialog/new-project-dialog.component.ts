import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material';
import {Project} from 'src/app/workspace/shared/model/project';
import {Subscription} from 'rxjs';
import {WorkspaceRestService} from '../shared/rest/workspace-rest.service';

@Component({
  selector: 'app-new-project-dialog',
  templateUrl: './new-project-dialog.component.html',
  styleUrls: ['./new-project-dialog.component.css']
})
export class NewProjectDialogComponent implements OnInit, OnDestroy {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  private projectSubscription: Subscription;

  constructor(
    public dialogRef: MatDialogRef<NewProjectDialogComponent>,
    private formBuilder: FormBuilder,
    private workspaceRestService: WorkspaceRestService
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  createProject() {
    const project: Project = new Project();
    project.name = this.firstFormGroup.get('projectName').value;
    project.calendar = null;
    project.dataSource = this.firstFormGroup.get('dataSource').value;
    this.projectSubscription = this.workspaceRestService
      .saveWorkspace(project)
      .subscribe();
  }

  inputIsValid(): boolean {
    return true;
    // this.firstFormGroup.valid && this.secondFormGroup.valid;
  }

  ngOnInit() {
    this.firstFormGroup = this.formBuilder.group({
      projectName: [
        '',
        Validators.compose([Validators.required, Validators.minLength(2)])
      ]
    });
  }
  ngOnDestroy(): void {
    if (this.projectSubscription) {
      this.projectSubscription.unsubscribe();
    }
  }
}
