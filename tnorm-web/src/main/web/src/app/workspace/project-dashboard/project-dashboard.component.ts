import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {Project} from 'src/app/workspace/shared/model/project';
import {NewProjectDialogComponent} from '../new-project-dialog/new-project-dialog.component';
import {CurrentWorkspaceService} from '../current-workspace.service';
import {WorkspaceRestService} from '../shared/rest/workspace-rest.service';

@Component({
  selector: 'app-project-dashboard',
  templateUrl: './project-dashboard.component.html',
  styleUrls: ['./project-dashboard.component.css']
})
export class ProjectDashboardComponent implements OnInit, OnDestroy {
  projects: Project[];
  private projectsSubscribtion: Subscription;

  constructor(public dialog: MatDialog,
              public workspaceRestService: WorkspaceRestService,
              private currentWorkspaceService: CurrentWorkspaceService,
              private router: Router) {
    this.getProjects();
  }

  private getProjects() {
    this.projectsSubscribtion = this.workspaceRestService
      .getWorkspaces()
      .subscribe(p => {
        this.projects = p;
        console.log(p)
      });
  }

  public openNewProjectDialog() {
    const dialogRef = this.dialog.open(NewProjectDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  public navigateToProjectWorkspace(project: Project) {
    this.currentWorkspaceService.currentProject$.next(project);
    this.router.navigateByUrl('/workspace');
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.projectsSubscribtion.unsubscribe();
  }
}
