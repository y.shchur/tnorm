import {Component, Input, OnInit} from '@angular/core';
import {Project} from 'src/app/workspace/shared/model/project';

@Component({
  selector: "app-project-tile",
  templateUrl: "./project-tile.component.html",
  styleUrls: ["./project-tile.component.css"]
})
export class ProjectTileComponent implements OnInit {
  @Input()
  project: Project;

  constructor() {}

  ngOnInit() {}
}
