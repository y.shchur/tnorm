import {DailyActivityType} from './daily-activity-type';

export class DailyActivityTypeExpression {
  public static readonly SCHOOL_YEAR = new DailyActivityTypeExpression('School year',DailyActivityType.SCHOOL_YEAR, 'school-year');
  public static readonly VACATION = new DailyActivityTypeExpression('Vacation', DailyActivityType.VACATION, 'vacation');
  public static readonly TRADE_ALLOWED = new DailyActivityTypeExpression('Trade allowed', DailyActivityType.TRADE_ALLOW, 'trade-allowed');
  public static readonly TRADE_PROHIBITED = new DailyActivityTypeExpression('Trade prohibited', DailyActivityType.TRADE_PROHIBITED, 'trade-prohibited');
  public static readonly HOLIDAY = new DailyActivityTypeExpression('Holiday',DailyActivityType.HOLIDAY, 'holiday');

  public static readonly DailyActivityTypeMap = new Map<DailyActivityType, DailyActivityTypeExpression>([
    [DailyActivityType.SCHOOL_YEAR, DailyActivityTypeExpression.SCHOOL_YEAR],
    [DailyActivityType.VACATION, DailyActivityTypeExpression.VACATION],
    [DailyActivityType.TRADE_ALLOW, DailyActivityTypeExpression.TRADE_ALLOWED],
    [DailyActivityType.TRADE_PROHIBITED, DailyActivityTypeExpression.TRADE_PROHIBITED],
    [DailyActivityType.HOLIDAY, DailyActivityTypeExpression.HOLIDAY]
  ]);

  private constructor(public readonly name: string, public readonly value: DailyActivityType, public readonly style: string) {}

  public static readonly AllValues: DailyActivityTypeExpression[] = [
    DailyActivityTypeExpression.SCHOOL_YEAR,
    DailyActivityTypeExpression.VACATION,
    DailyActivityTypeExpression.TRADE_ALLOWED,
    DailyActivityTypeExpression.TRADE_PROHIBITED,
    DailyActivityTypeExpression.HOLIDAY
  ];
}
