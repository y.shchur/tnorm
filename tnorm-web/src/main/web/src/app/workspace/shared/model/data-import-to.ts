import {TableModel} from '../../../scheduling/shared/model/table-model';

export class TableImportTo {
  file: File;
  encoding: string;
  tableModel: TableModel;
  constructor(file: File, encoding: string, tableModel: TableModel) {
    this.file = file;
    this.tableModel = tableModel;
    this.encoding = encoding;
  }
}
