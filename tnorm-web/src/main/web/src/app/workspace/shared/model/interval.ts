import {NgbDate} from '@ng-bootstrap/ng-bootstrap';
import {DailyActivityType} from './daily-activity-type';

export class Interval {
  constructor(start: NgbDate, end: NgbDate, intervalType: DailyActivityType) {
    this.start = start;
    this.end = end;
    this.intervalType = intervalType;
  }

  start: NgbDate;
  end: NgbDate;
  intervalType: DailyActivityType;

  static checkDateIsInInterval(date: NgbDate, interval: Interval) {
    return (date.after(interval.start) && date.before(interval.end)) || date.equals(interval.start) || date.equals(interval.end);
  }
}
