import {Calendar} from './calendar';

export class Project {
  constructor() {}

  id: string;
  name: string;
  calendar: Calendar;
  dataSource: string;
}
