import {Injectable} from '@angular/core';
import {CurrentWorkspaceService} from '../../current-workspace.service';
import {HttpClient} from '@angular/common/http';
import {AbstractRestService} from '../../../shared/service/abstract-rest-service';
import {Observable} from 'rxjs';
import {Project} from '../model/project';
import {WorkspaceUrlConstants} from './workspace-url-constants';

@Injectable({
  providedIn: 'root'
})
export class WorkspaceRestService extends AbstractRestService {
  private currentProject: Project;

  constructor(readonly projectService: CurrentWorkspaceService, readonly http: HttpClient) {
    super(projectService);
    projectService.currentProject$.subscribe(p => (this.currentProject = p));
  }

  getWorkspaces(): Observable<Project[]> {
    return this.http.get<Project[]>(WorkspaceUrlConstants.ALL_WORKSPACES).pipe(p => p);
  }

  saveCurrentWorkspace(): Observable<Project> {
    return this.http.post<Project>(WorkspaceUrlConstants.API_WORKSPACE, this.currentProject, this.httpOptions);
  }

  saveWorkspace(project: Project): Observable<Project> {
    return this.http.post<Project>(WorkspaceUrlConstants.API_WORKSPACE, project, this.httpOptions);
  }
}
