export class WorkspaceUrlConstants {
  static readonly API_WORKSPACE: string = '/api/workspace';
  static readonly ALL_WORKSPACES: string = WorkspaceUrlConstants.API_WORKSPACE + '/workspaces';
}
