import {Component, OnInit} from '@angular/core';
import {CurrentWorkspaceService} from './current-workspace.service';
import {ImportDataService} from '../scheduling/import/import-data.service';
import {MatDialog} from '@angular/material';
import {ImportDataDialogComponent} from '../scheduling/import/import-data-dialog/import-data-dialog.component';
import {CalendarDialogComponent} from './calendar-dialog/calendar-dialog.component';

@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.css']
})
export class WorkspaceComponent implements OnInit {
  constructor(public dialog: MatDialog, private projectService: CurrentWorkspaceService, private workspaceService: ImportDataService) {
  }
  ngOnInit() {}

  public openImportDataDialog() {
    this.dialog.open(ImportDataDialogComponent);
  }

  public openCalendarDialog() {
    this.dialog.open(CalendarDialogComponent);
  }
}
